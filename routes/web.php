<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

 /**
  * Recomiendo:
  * Rutas parte 1: https://parzibyte.me/blog/2019/02/14/explicando-rutas-web-laravel-5-7/
  * Rutas parte 2: https://parzibyte.me/blog/2019/02/20/rutas-laravel-parte-2-prefijos-fallback-limite-de-tasa-formularios/
  */
# Cuando se solicita la url /, es decir, la raíz
Route::get('/', "TiendaController@index")
->name("inicio");

# Mostrar el formulario para agregar
Route::view("/agregar", "nueva_tienda")
->name("formAgregar");

Route::post("/agregar", "TiendaController@agregarTienda")
->name("agregarTienda");

# Cuando se guardan los cambios en la base de datos
Route::post("/guardarCambios", "TiendaController@guardarCambiosDeTienda")
    ->name("guardarCambiosDeTienda");

# Mostrar el formulario para editar la canción, algo como editar/1
Route::get("/editar/{id}", "TiendaController@editarTienda")
    ->name("editarTienda");

# URL que es llamada para eliminar canción, algo como eliminar/1
Route::get("/eliminar/{id}", "TiendaController@eliminarTienda")
    ->name("eliminarTienda");



#PRODUCTO
Route::get('/productos', "ProductoController@index")
->name("producto");
#TBLproducto
#Envio de formularios de producto
Route::view("/agregarproducto", "nuevo_producto")
->name("formAgregar_producto");

Route::get("/agregarNuevoproducto", "ProductoController@getTiendas")
->name("formAgregar_producto");

Route::post("/agregarproducto", "ProductoController@agregarProducto")
->name("agregarProducto");


# Mostrar el formulario para editar el producto, algo como editar/1
Route::get("/editarproducto/{id}", "ProductoController@editarProducto")
->name("editarProducto");

# Cuando se guardan los cambios en la base de datos
Route::post("/guardarCambiosproducto", "ProductoController@guardarCambiosDeProducto")
->name("guardarCambiosDeProducto");

# URL que es llamada para eliminar canción, algo como eliminar/1
Route::get("/eliminarproducto/{id}", "ProductoController@eliminarProducto")
->name("eliminarProducto");