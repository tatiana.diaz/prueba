<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tienda extends Model
{
    protected $table = "tienda";
    # No queremos que ponga updated_at ni created_at
    public $timestamps = false;
}

