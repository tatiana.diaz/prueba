<?php

namespace App\Http\Controllers;

/*
    Si no sabes de dónde viene el nombre de las
    tablas y en dónde estamos confugrando las credenciales
    mira el archivo .env y database/esquema.sql

    También echa un vistazo a las migraciones
*/

# Queremos acceder a la petición HTTP
use Illuminate\Http\Request;

# También queremos al modelo producto
use App\producto;
use App\Tienda;


class ProductoController extends Controller
{

    // Devolver la vista con todas las employeees
    public function index()
    {
        $producto = producto::get();

        return view("producto")
            ->with("producto", $producto );
    }
    function getTiendas(){
        $tiendas = Tienda::get();
        return view("nuevo_producto")
        ->with("tiendas",$tiendas);
    }

    public function agregarProducto(Request $peticion)
    {
        $sku ="";
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        # Crear un modelo...
        $producto = new producto;
        
        #metodo para crear un SKU  
        $max = strlen($pattern)-1;
        $input_length = strlen($pattern);
        for($i = 0; $i < 20; $i++) {
            $random_character = $pattern[mt_rand(0, $input_length - 1)];
            $sku .= $random_character;
        }
        //Ejemplo de uso
        #metodo valida SKU no exista
        
        
        # Establecer propiedades leídas del formulario
        $producto->nombre_pro = $peticion->nombre_pro;
        $producto->sku = strtoupper($sku);
        $producto->descripcion = $peticion->descripcion;
        $producto->valor = $peticion->valor;
        $producto->tienda = $peticion->tienda;
        $producto->imagen = $peticion->imagen;
        
        # Y guardar modelo ;)
        $producto->save();

        #Ahora redirige a la ruta con el nombre
        #inicio (mira routes/web.php) y pásale
        #un mensaje en la variable "mensaje" con
        #el valor de "Canción agregada"
        
        return redirect()
            ->route('producto')
            ->with('mensaje', 'Producto agregado');
    }

    public function editarProducto(Request $peticion)
    {
        $idproducto = $peticion->route("id");
        $producto = producto::get();
        # Obtener canción por ID o fallar, es decir, mostrar un 404
        $producto = producto::findOrFail($idproducto);
        
        return view("editar_producto")
            ->with("producto", $producto);
    }

    public function guardarCambiosDeProducto(Request $peticion)
    {
        
        # El id para el where de SQL
        $idproducto = $peticion->idproducto;
        # Obtener modelo fresco de la base de datos
        # Buscar o fallar
        $producto= producto::findOrFail($idproducto);
        # Los nuevos datos
        $producto->nombre_pro = $peticion->nombre_pro;
        $producto->descripcion = $peticion->descripcion;
        $producto->valor = $peticion->valor;
        $producto->tienda = $peticion->tienda;
        $producto->imagen = $peticion->imagen;
        # Y guardamos ;);
        
        $producto->save();
        
        /*
        Ahora redirige a la ruta con el nombre
        inicio (mira routes/web.php) y pásale
        un mensaje en la variable "mensaje" con
        el valor de "Canción actualizada"
         */
        return redirect()
            ->route('producto')
            ->with('mensaje', 'producto actualizada');

    }

    public function eliminarProducto(Request $peticion)
    {
        # El id para el where de SQL
        $idproducto = $peticion->route("id");
        # Obtener canción o mostrar 404
        $producto = producto::findOrFail($idproducto);
        # Eliminar
        $producto->delete();
        
        #Ahora redirige a la ruta con el nombre
        #inicio (mira routes/web.php) y pásale
        #un mensaje en la variable "mensaje" con
        #el valor de "Canción eliminada"
        
        return redirect()
            ->route('producto')
            ->with('mensaje', 'Producto eliminado');
    }
}
