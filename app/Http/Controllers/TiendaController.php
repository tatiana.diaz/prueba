<?php

namespace App\Http\Controllers;

/*
    Si no sabes de dónde viene el nombre de las
    tablas y en dónde estamos confugrando las credenciales
    mira el archivo .env y database/esquema.sql

    También echa un vistazo a las migraciones
*/

# Queremos acceder a la petición HTTP
use Illuminate\Http\Request;

# También queremos al modelo tienda
use App\Tienda;


class TiendaController extends Controller
{

    // Devolver la vista con todas las employeees
    public function index()
    {
        $tienda = Tienda::get();

        return view("inicio")
            ->with("tiendas", $tienda);
    }

    public function agregarTienda(Request $peticion)
    {

        # Crear un modelo...
        $tienda = new Tienda;

        # Establecer propiedades leídas del formulario
        $tienda->nombre = $peticion->nombre;
        $tienda->fecha_de_apertura = $peticion->fecha_de_apertura;
       
        # Y guardar modelo ;)
        $tienda->save();
    
        #Ahora redirige a la ruta con el nombre
        #inicio (mira routes/web.php) y pásale
        #un mensaje en la variable "mensaje" con
        #el valor de "Canción agregada"
        
        return redirect()
            ->route('inicio')
            ->with('mensaje', 'Tienda agregad');
    }

    public function editarTienda(Request $peticion)
    {
        $idTienda = $peticion->route("id");
        $tienda = Tienda::get();
        # Obtener canción por ID o fallar, es decir, mostrar un 404
        $tienda = Tienda::findOrFail($idTienda);
        
        return view("editar_tienda")
            ->with("tienda", $tienda);
    }

    public function guardarCambiosDeTienda(Request $peticion)
    {
        # El id para el where de SQL
        $idTienda = $peticion->idTienda;
        # Obtener modelo fresco de la base de datos
        # Buscar o fallar
        $Tienda = Tienda::findOrFail($idTienda);
        # Los nuevos datos
        $Tienda->nombre = $peticion->nombre;
        $Tienda->Fecha_de_apertura = $peticion->fecha_de_apertura;
        # Y guardamos ;);
        $res = $Tienda->save();
        
        /*
        Ahora redirige a la ruta con el nombre
        inicio (mira routes/web.php) y pásale
        un mensaje en la variable "mensaje" con
        el valor de "Canción actualizada"
         */
        return redirect()
            ->route('inicio')
            ->with('mensaje', 'Tienda actualizada');

    }

    public function eliminarTienda(Request $peticion)
    {
        # El id para el where de SQL
        $idTienda = $peticion->route("id");
        # Obtener canción o mostrar 404
        $tienda = Tienda::findOrFail($idTienda);
        # Eliminar
        $tienda->delete();
        
        #Ahora redirige a la ruta con el nombre
        #inicio (mira routes/web.php) y pásale
        #un mensaje en la variable "mensaje" con
        #el valor de "Canción eliminada"
        
        return redirect()
            ->route('inicio')
            ->with('mensaje', 'Tienda eliminada');
    }
}
