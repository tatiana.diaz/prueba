<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class producto extends Model
{
    protected $table = "producto";
    # No queremos que ponga updated_at ni created_at
    public $timestamps = false;
}