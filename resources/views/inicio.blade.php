@extends("principal")
@section("titulo", "Tiendas")

@section("contenido")
<h1>Lista Tiendas</h1> 
<br>
<br>
@if(session("mensaje"))
<h3 style="color: blue;">{{ session("mensaje") }}</h3>
@endif
<a href="{{route('formAgregar')}}">Agregar Tienda</a>
<br><br>
<table class="table table-dark">
    <thead>
        <tr>
            <th>id</th>
            <th>nombre</th>
            <th>Fecha de apertura</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    @foreach ($tiendas as $item)
    <tr>
        <td>{{ $item->id }}</td>
        <td>{{ $item->nombre }}</td>
        <td>{{ $item->fecha_de_apertura }}</td>
        <td>
            <a href="{{ route('editarTienda', ['id' => $item->id]) }}">Editar</a>
        </td>
        <td>
            <a href="{{ route('eliminarTienda', ['id' => $item->id]) }}">Eliminar</a>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
@endsection
