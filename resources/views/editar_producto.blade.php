@extends("principal")
@section("titulo", "Editar Producto")

@section("contenido")
<h1>Editar Producto</h1>
<form method="POST" action="{{ route('guardarCambiosDeProducto') }}">
    @csrf
    <input value="{{ $producto->id }}" type="hidden" name="idproducto">
    <label>Nombre:</label>
    <input type="text" name="nombre_pro" value="{{ $producto->nombre_pro }}"required>
    <br><br>
    <label>Descripcion:</label>
    <br>
    <textarea  name="descripcion" style="resize:none;" cols="32" rows="10" value="">{{ $producto->descripcion }}</textarea>
    <br><br>
    <label>Valor :</label>
    <input type="number" name="valor" value="{{ $producto->valor }}"required>
    <br><br>
    <label for="">Archivo:</label> 
    <input type="text" name="imagen" value="{{ $producto->imagen }}"required>
    <br><br>
    <label for="">Tienda:</label> 
    <br>
    <label for="">Tienda asignada:</label><input type="text" value="{{ $producto->tienda }}" disabled>
    <br>
    <br>
    <select name="tienda" id="tienda" required>
    <option value="" selected disabled>Seleccione...</option>
    <option value="1">Exito</option>
    <option value="2">Falabella</option>
    </select>
    <br><br>
    <input type="submit" value="Guardar datos"></p>
</form>
@endsection
