@extends("principal")
@section("titulo", "Editar Tienda")

@section("contenido")
<h1>Editar Tienda</h1>
<form method="POST" action="{{ route('guardarCambiosDeTienda') }}">
    @csrf
    <input value="{{ $tienda->id }}" type="hidden" name="idTienda">
    <input autocomplete="off" required value="{{ $tienda->nombre }}" type="text" name="nombre" placeholder="Nombre nuevo">
    <br><br>
    <input autocomplete="off" required value="{{ $tienda->fecha_de_apertura }}" type="date" name="fecha_de_apertura" >
    <br><br>
    <input type="submit" value="Guardar">
</form>
@endsection
