@extends("principal")
@section("titulo", "Agregar Tienda")

@section("contenido")
<h1>Agregar Tienda</h1>
@if(session("mensaje"))
<h3 style="color: blue;">{{ session("mensaje") }}</h3>
@endif
<form method="POST" action="{{ route('agregarTienda') }}">
    @csrf
    <input autocomplete="off" required type="text" name="nombre" placeholder="Nombres">
    <br><br>
    <label for="">Fecha de apertura</label>
    <br>
    <input autocomplete="off" required type="date" name="fecha_de_apertura">
    <br><br>
    <input type="submit" value="Guardar">
</form>
@endsection