@extends("principal")
@section("titulo", "Producto")

@section("contenido")
<h1>Lista de Productos</h1>
@if(session("mensaje"))
<h3 style="color: blue;">{{ session("mensaje") }}</h3>
@endif
<a href="{{route('formAgregar_producto')}}">Agregar Producto</a>
<br><br>
<table>
    <thead>
        <tr>
            <th>id</th>
            <th>nombre</th>
            <th>sku</th>
            <th>descripcion</th>
            <th>Valor</th>
            <th>tienda</th>
            <th>imagen</th>
            <th>Editar</th>
            <th>Eliminar</th>
        </tr>
    </thead>
    <tbody>
    @foreach ($producto as $item)
    <tr>
        <td>{{ $item->id }}</td>
        <td>{{ $item->nombre_pro }}</td>
        <td>{{ $item->sku }}</td>
        <td>{{ $item->descripcion }}</td>
        <td>{{ $item->valor }}</td>
        <td>{{ $item->tienda }}</td>
        <td>{{ $item->imagen }}</td>
        <td>
            <a href="{{ route('editarProducto', ['id' => $item->id]) }}">Editar</a>
        </td>
        <td>
            <a href="{{ route('eliminarProducto', ['id' => $item->id]) }}">Eliminar</a>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>
@endsection
