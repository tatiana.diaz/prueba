@extends("principal")
@section("titulo", "Agregar Cargo")

@section("contenido")
<h1>Agregar producto</h1>
@if(session("mensaje"))
<h3 style="color: blue;">{{ session("mensaje") }}</h3>
@endif
<form method="POST" action="{{ route('agregarProducto') }}">
    @csrf  
    <label>Nombre:</label>
    <input type="text" name="nombre_pro" required>
    <br><br>
    <label>Descripcion:</label>
    <br>
    <textarea  name="descripcion" style="resize:none;" cols="32" rows="10" required></textarea>
    <br><br>
    <label>Valor :</label>
    <input type="number" name="valor" value="" required>
    <br><br>
    <label for="">Archivo:</label> 
    <input type="text" value="archivo" name="imagen" required>
    <br><br>
    <label for="">Tienda:</label> 
    <select name="tienda" id="tienda" required>
    @foreach ($tiendas as $item)
        <option value="{{$item['id']}}">{{$item['nombre']}}</option>
    @endforeach
    </select>
    <br><br>
    <input type="submit" value="Enviar datos"></p>
</form>
@endsection